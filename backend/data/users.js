import bcrypt from "bcryptjs";

const users = [
  {
    name: "Admin user",
    email: "admin@test.com",
    password: bcrypt.hashSync("test", 10),
    isAdmin: true
  },
  {
    name: "John Doe",
    email: "john@test.com",
    password: bcrypt.hashSync("test", 10)
  },
  {
    name: "Jane Doe",
    email: "jane@test.com",
    password: bcrypt.hashSync("test", 10)
  }
];

export default users;
